import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cobertura } from './cobertura-detail.model';

@Component({
  selector: 'app-cobertura-data',
  templateUrl: './cobertura-data.component.html',
  styles: []
})
export class CoberturaDataComponent {
  public formData: Cobertura[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Cobertura[]>(baseUrl + 'api/Cobertura').subscribe(result => {
      this.formData = result;
    }, error => console.error(error));
  }
}


