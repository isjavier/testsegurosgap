export class Cobertura {
  coberturaId: number;
  cobertura: string;
  fechaCreacion: Date;
  fechaModificacion: Date;
}
