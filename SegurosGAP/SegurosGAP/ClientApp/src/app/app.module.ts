import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { UsuarioLoginComponent } from './usuario-data/usuario-data-login/usuario-login-data-component';
import { ClienteDataComponent } from './cliente-data/cliente-data.component';

import { PolizaDataComponent } from './poliza-data/poliza-data.component';
import { PolizaDetailComponent } from './poliza-data/poliza-detail/poliza-detail.component';
import { PolizaDataListComponent } from './poliza-data/poliza-data-list/poliza-data-list.component';
import { PolizaDetailService } from './poliza-data/shared/poliza-detail.service';


import { ClientePolizaDetailService } from "./cliente-poliza/shared/cliente-poliza-detail.service"
import { CPDataComponent } from "./cliente-poliza/cp-data.component"
import { CPDetailComponent } from "./cliente-poliza/cp-detail/cp-detail.component"
import { CPDataListComponent } from "./cliente-poliza/cp-data-list/cp-data-list.component"


import { CoberturaDataComponent } from './cobertura-data/cobertura-data.component';
import { UsuarioDataComponent } from './usuario-data/usuario-data.component';



@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    UsuarioLoginComponent,
    PolizaDataComponent,
    PolizaDetailComponent,
    PolizaDataListComponent,
    CoberturaDataComponent,
    UsuarioDataComponent,
    ClienteDataComponent,
    CPDataComponent,
    CPDataListComponent,
    CPDetailComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'usuario-data-login', component: UsuarioLoginComponent },
      { path: 'cliente-data', component: ClienteDataComponent },
      { path: 'poliza-data', component: PolizaDataComponent },
      { path: 'cobertura-data', component: CoberturaDataComponent },
      { path: 'usuario-data', component: UsuarioDataComponent },
      { path: 'cp-data', component: CPDataComponent },

    ]),
  ],
  providers: [PolizaDetailService, ClientePolizaDetailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
