import { Component, OnInit } from '@angular/core';
import { PolizaDetailService } from '../shared/poliza-detail.service';
import { Poliza } from '../shared/poliza-detail.model';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-poliza-data-list',
  templateUrl: './poliza-data-list.component.html',
  styles: []
})
export class PolizaDataListComponent implements OnInit {


  constructor(private service: PolizaDetailService) { }

  ngOnInit() {
    this.service.RefrescarListaPolizas();
  }

  cargarForm(frmData: Poliza) {
   this.service.formData = Object.assign({}, frmData);
   this.RefrescarCboCoberturas();
    this.service.ObtenerPolizaCoberturas();
    this.service.HabilitarYDeshabilitarCoberturas(false);
  }

  RefrescarCboCoberturas() {
    var DropdownList = document.getElementById("cboCobertura") as HTMLSelectElement;
    if (DropdownList  != undefined && DropdownList  != null) {
      for (let i = 0; i < DropdownList.options.length; i++) {
        DropdownList.options[i].selected = false;
      }
    }
  }

  EliminarPoliza(polizaId) {
    this.service.DeleteBorrarPoliza(polizaId)
      .subscribe(res => {
        this.service.RefrescarListaPolizas();
        this.service.formData;
        console.log("eliminada");
      },
        err => {
          console.log(err);
        }
      )}
}

