import { Component, OnInit } from '@angular/core';
import { PolizaDetailService } from '../shared/poliza-detail.service';
import { NgForm } from '@angular/forms';
import { Poliza } from '../shared/poliza-detail.model';
import { Poliza_Cobertura } from '../shared/poliza-cobertura-detail.model';

@Component({
  selector: 'app-poliza-detail',
  templateUrl: './poliza-detail.component.html',
  styles: []
})

export class PolizaDetailComponent implements OnInit {
  mensajeAccion: string;
  polizaCoberturas = new  Array;

  constructor(private service: PolizaDetailService) {
  }

  ngOnInit() {
    this.resetForm();
    this.service.ObtenerCoberturas();
  }

  resetForm(form?: NgForm) {
    this.service.HabilitarYDeshabilitarCoberturas(true);
    if (form != null)
      form.resetForm();
    this.service.formData = {
      polizaId: 0,
      nombrePoliza: '',
      descripcion: '',
      precioMensualDolares: null,
      porcentajeCobertura: null,
      tipoRiesgoId: null
    }}

  onSubmit(form: NgForm) {
    this.ObtenerTipoRiesgo();
    if (this.service.formData.polizaId == 0) {
      if (this.ValidarFormulario(this.service.formData)) {
        this.AgregarPoliza(form);
        
      }
    }
    else {
      if (this.ValidarFormulario(this.service.formData)) {
        this.ActualizarPoliza(form);
        
      }
    }
  }

  AgregarPoliza(form: NgForm) {
    this.service.PostAgregarPoliza().subscribe(
      response => {
        //this.AgregarPolizaCobertura();
        this.resetForm(form);
        this.service.RefrescarListaPolizas();
        this.mensajeAccion = "Póliza agregada.";
      },
      err => {
        console.log(err);
      })}

  ActualizarPoliza(form: NgForm) {
    this.service.PutActualizarPoliza().subscribe(
      response => {
        this.AgregarPolizaCobertura();
        this.resetForm(form);
        this.service.RefrescarListaPolizas();
        this.mensajeAccion = "Póliza actualizada.";
      },
      err => {
        console.log(err);
      })}

  AgregarPolizaCobertura() {
    this.service.PostAgregarPolizaCoberturas(this.obtenerPolizasCoberturasSeleccionadas()).subscribe(
    respose => {
      this.polizaCoberturas = [];
        alert(this.mensajeAccion);
      },
    err => {
      this.polizaCoberturas = [];
        console.log(err);
      })}


obtenerPolizasCoberturasSeleccionadas() {
    //var arr = []
  if (this.polizaCoberturas.length <= 0) {
    var cobertura = new Poliza_Cobertura;
    var DropdownList = document.getElementById("cboCobertura") as HTMLSelectElement;
    if (DropdownList != undefined && DropdownList != null) {
      for (let i = 0; i < DropdownList.options.length; i++) {
        if (DropdownList.options[i].selected) {
          cobertura.polizaId = this.service.formData.polizaId;
          cobertura.coberturaId = Number(DropdownList.options[i].value);
          this.polizaCoberturas.push(cobertura);
        }
      }
    }
  }
  return this.polizaCoberturas;
}

  //Obtener riesgo del select option
  ObtenerTipoRiesgo() {
    var cboRiesgo = document.getElementById("cboRiesgo") as HTMLSelectElement;
    var opcion = cboRiesgo.options[cboRiesgo.selectedIndex].value;
    this.service.formData.tipoRiesgoId = Number(opcion);
  }


  //Validaciones generales del formulario.

  ValidarFormulario(poliza: Poliza) {
    var mensaje = "";
    var isvalid = true;
    if (!this.service.ValidarReglaRiesgoPorcentage()) {
      isvalid = false;
      mensaje = "Cuando el riesgo es Alto el porcentaje de la póliza no puede exceder más del 50%.";
    }
    else {
      mensaje = "Verificar valores del formulario.";
      if (!this.service.ValidarCampoFormGeneral(poliza.nombrePoliza)) {
        isvalid = false;
      }
      if (!this.service.ValidarCampoFormGeneral(poliza.descripcion)) {
        isvalid = false;
      }
      if (!this.service.ValidarCampoFormGeneral(poliza.porcentajeCobertura)) {
        isvalid = false;
      }
      if (!this.service.validarPorcentage()) {
        isvalid = false;
      }
      if (!this.service.ValidarCampoFormGeneral(poliza.precioMensualDolares)) {
        isvalid = false;
      }
      if (!this.service.ValidarCampoFormGeneral(poliza.tipoRiesgoId)) {
        isvalid = false;
      }
    }
    if (!isvalid) {
      alert(mensaje);
      return false;
    } else {
      return true;
    }}



}
