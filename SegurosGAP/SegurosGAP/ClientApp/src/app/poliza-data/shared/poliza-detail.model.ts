export class Poliza {
  polizaId: number;
  nombrePoliza: string;
  descripcion: string;
  precioMensualDolares: number;
  porcentajeCobertura: number;
  tipoRiesgoId: number;
}
