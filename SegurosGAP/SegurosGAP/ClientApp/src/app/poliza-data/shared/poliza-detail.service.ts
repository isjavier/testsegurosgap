import { Poliza } from './poliza-detail.model';
import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { Cobertura } from '../../cobertura-data/cobertura-detail.model';
import { Poliza_Cobertura } from './poliza-cobertura-detail.model';

@Injectable()
export class PolizaDetailService {
  formData: Poliza;
  readonly BASE_URL: string;
  list: Poliza[];
  listCobertura: Cobertura[];
  listPolizaCobertura: Poliza_Cobertura[]

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.BASE_URL = baseUrl;
  }
 
  PostAgregarPoliza() {
    return this.http.post(this.BASE_URL + 'api/Poliza/', this.formData);
  }

  PostAgregarPolizaCoberturas(listCoberturas: Poliza_Cobertura[]) {
    return this.http.post(this.BASE_URL + 'api/PolizaCobertura/', listCoberturas);
  }

  RefrescarListaPolizas() {
    return this.http.get(this.BASE_URL + 'api/Poliza')
      .toPromise()
      .then(res => this.list = res as Poliza[]);
  }

  PutActualizarPoliza() {
    return this.http.put(this.BASE_URL + 'api/Poliza/'+this.formData.polizaId, this.formData);
  }

  DeleteBorrarPoliza(id) {
    return this.http.delete(this.BASE_URL + 'api/Poliza/' + id);
  }

  //Lenar el combo de tipo cobertura
  ObtenerCoberturas() {
    return this.http.get(this.BASE_URL + 'api/Cobertura')
      .toPromise()
      .then(res => this.listCobertura = res as Cobertura[]);
  }

  //Obtener todas las coberturas por poliza
  ObtenerPolizaCoberturas() {
    return this.http.get(this.BASE_URL + 'api/PolizaCobertura/'+this.formData.polizaId)
      .subscribe(result => {
        this.listPolizaCobertura = result as Poliza_Cobertura[];
        var cboCobertura = document.getElementById("cboCobertura") as HTMLSelectElement;       
          for (let i = 0; i < cboCobertura.options.length; i++) {
            for (let j = 0; j < this.listPolizaCobertura.length; j++) {
              if (Number(cboCobertura.options[i].value) == this.listPolizaCobertura[j].coberturaId) {
                cboCobertura.options[i].selected = true;
              }
            }
        }  
      }, error => console.error(error));
  }

  //Validaciones generales
  ValidarCampoFormGeneral(value) {
    if (value != undefined && value != null && value != '') {
      return true;
    }
    return false;
  }

  validarPorcentage() {
    if (this.formData.porcentajeCobertura > 1 && this.formData.porcentajeCobertura < 99) {
      return true;
    }
    return false;
  }

  ValidarReglaRiesgoPorcentage() {
    if (this.formData.porcentajeCobertura > 50 && this.formData.tipoRiesgoId == 4) {
      return false;
    }
    return true;
  }


  HabilitarYDeshabilitarCoberturas(action) {
    var cboCobertura = document.getElementById("cboCobertura") as HTMLSelectElement;
    cboCobertura.disabled = action;
  }

  
}
