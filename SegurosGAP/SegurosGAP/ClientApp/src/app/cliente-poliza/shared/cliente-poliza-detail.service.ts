import { DetalleClientePoliza } from './cliente-poliza-detail.model';
import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { Poliza } from '../../poliza-data/shared/poliza-detail.model';



@Injectable()
export class ClientePolizaDetailService {
  formData: DetalleClientePoliza;
  readonly BASE_URL: string;
  listDetalleCP: DetalleClientePoliza[];
  listPoliza: Poliza[];


  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.BASE_URL = baseUrl;
  }


  PostAgregarClientePoliza() {
    this.ObtenerPoliza();
    if (this.validarFormulario()) {
      this.formData.estadoPoliza = true;
      return this.http.post(this.BASE_URL + 'api/ClientePoliza/', this.formData);
    }
  }


//Obtener cliente-polizas
  RefrescarListaClientePoliza() {
    return this.http.get(this.BASE_URL + 'api/ClientePoliza')
      .toPromise()
      .then(res => this.listDetalleCP = res as DetalleClientePoliza[]);
  }


  //Lenar el combo de pólizas
  ObtenerPolizas() {
    return this.http.get(this.BASE_URL + 'api/Poliza')
      .toPromise()
      .then(res => this.listPoliza = res as Poliza[]);
  }

  //verificar usuario
  verificarUsuarioPorIdentificacion(value) {
    for (let i = 0; i < this.listDetalleCP.length;i++) {
      if (this.listDetalleCP[i].identificacion == value) {
        this.formData.nombreCliente = this.listDetalleCP[i].nombreCliente;
        this.formData.clienteId = this.listDetalleCP[i].clienteId;
        return true;
      }
    }
    alert("Identificación no valida.");
    return false;
  }

  //Obtener informacion Póliza
  ObtenerPoliza() {
    var polizaId;
    var DropdownList = document.getElementById("cboPoliza") as HTMLSelectElement;
    for (let i = 0; i < DropdownList.length; i++) {
      if (DropdownList.options[i].selected) {
        polizaId = Number(DropdownList.options[i].value);
      }
    }

    for (let i = 0; i < this.listPoliza.length; i++) {
      if (this.listPoliza[i].polizaId == polizaId) {
        this.formData.polizaId = this.listPoliza[i].polizaId;
        this.formData.nombrePoliza = this.listPoliza[i].nombrePoliza;
        this.formData.precioMensualDolares = this.listPoliza[i].precioMensualDolares;
        this.formData.porcentajeCobertura = this.listPoliza[i].porcentajeCobertura;
        return true;
      }
    }
  }

  DeleteRemoverClientePoliza(id) {
    return this.http.delete(this.BASE_URL + 'api/ClientePoliza/' + id);
  }

  validarFormulario() {
    if (this.formData.nombreCliente != null && this.formData.nombreCliente.length != 0 && this.formData.nombreCliente != '') {
      return true;
    } else {
      alert("No todos los datos esta correctamente ingresados.");
      return false;
    }
  }

}
