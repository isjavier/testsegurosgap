export class DetalleClientePoliza {
  clientePolizaId: number;
  inicioVigencia: Date;
  periodoCobertura: number;
  precioTotalDolares: number;
  estadoPoliza: boolean;
  clienteId: number;
  nombreCliente: string;
  identificacion: string;
  polizaId: number;
  nombrePoliza: string;
  porcentajeCobertura: number;
  precioMensualDolares: number;

}  
