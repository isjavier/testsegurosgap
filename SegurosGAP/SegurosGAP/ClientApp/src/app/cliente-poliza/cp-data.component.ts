import { Component, OnInit } from '@angular/core';
import { ClientePolizaDetailService } from './shared/cliente-poliza-detail.service';

@Component({
  selector: 'app-cp-data',
  templateUrl: './cp-data.component.html',
  styles: []
})


export class CPDataComponent implements OnInit {

  constructor(private service: ClientePolizaDetailService) { }

  ngOnInit() {
  }

}
