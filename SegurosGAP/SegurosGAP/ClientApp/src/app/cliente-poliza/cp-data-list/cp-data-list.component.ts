import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ClientePolizaDetailService } from '../shared/cliente-poliza-detail.service';
import { DetalleClientePoliza } from '../shared/cliente-poliza-detail.model';


@Component({
  selector: 'app-cp-data-list',
  templateUrl: './cp-data-list.component.html',
  styles: []
})
export class CPDataListComponent implements OnInit {


  constructor(private service: ClientePolizaDetailService) { }

  ngOnInit() {
    this.service.RefrescarListaClientePoliza();
  }

  cargarForm(frmData: DetalleClientePoliza) {
    alert("Edición de datos no disponible.");
  }

  
  EliminarClientePoliza(clientePolizaId) {
    this.service.DeleteRemoverClientePoliza(clientePolizaId)
      .subscribe(res => {
        this.service.RefrescarListaClientePoliza();
        console.log("eliminada");
      },
        err => {
          console.log(err);
        }
      )}
}

