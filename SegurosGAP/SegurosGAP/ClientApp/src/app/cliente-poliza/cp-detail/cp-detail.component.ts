import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ClientePolizaDetailService } from '../shared/cliente-poliza-detail.service';

@Component({
  selector: 'app-cp-detail',
  templateUrl: './cp-detail.component.html',
  styles: []
})

export class CPDetailComponent implements OnInit {
//mensajeAccion: string;

  constructor(private service: ClientePolizaDetailService) {
  }

  ngOnInit() {
    this.resetForm();
    this.service.ObtenerPolizas();
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.formData = {
      clientePolizaId: 0,
      inicioVigencia: null,
      periodoCobertura: null,
      precioTotalDolares:0,
      estadoPoliza: null,
      clienteId: 0,
      nombreCliente: '',
      identificacion:'',
      polizaId: 0,
      nombrePoliza: '',
      porcentajeCobertura: null,
      precioMensualDolares: null 
    }
  }

  verificarCliente(value) {
    this.service.verificarUsuarioPorIdentificacion(value);
  }


  onSubmit(form: NgForm) {
  this.service.PostAgregarClientePoliza().subscribe(
      response => {
        this.resetForm(form);
        this.service.RefrescarListaClientePoliza();
        alert( "Póliza agregada.");
      },
      err => {
        console.log(err);
      })   
  }
}
