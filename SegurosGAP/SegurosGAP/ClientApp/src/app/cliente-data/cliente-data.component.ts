import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-cliente-data',
  templateUrl: './cliente-data.component.html',
  styles: []
})
export class ClienteDataComponent {
  public formData: Cliente[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Cliente[]>(baseUrl + 'api/Cliente').subscribe(result => {
      this.formData = result;
    }, error => console.error(error));
  }
}

interface Cliente {
  clienteId: number;
  nombreCliente: string,
  email: string,
  telefono: string,
  direccion: string;
}


