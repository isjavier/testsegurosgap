import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-login-data',
  templateUrl: './usuario-login-component.html',
  styles: []
})


export class UsuarioLoginComponent implements OnInit{
  public Usuario: LoginUsuario;
  readonly ctrlURL: string;


  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.ctrlURL = baseUrl;
  }

  onSubmit(form: NgForm) {
    this.PostUsuarioLogin(form.value).subscribe(
      response => {
        if (response) {
          this.ModificarNavigator('block');
        } else {
          alert("Email o Password incorrectos.");
        }
        this.resetForm(form);
      },
      err => {
        console.log(err);
      }
    )
  }
 
  PostUsuarioLogin(Usuario: LoginUsuario) {
    return this.http.post(this.ctrlURL + 'api/Usuario', Usuario);
 }

  ngOnInit() {
    this.ModificarNavigator('none')
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.Usuario = {
      email: '',
      password: ''
    }
  }

  ModificarNavigator(action) {
    document.getElementById("ulNavigatorGAP").style.display = action;
  }

}

 interface LoginUsuario{
  email: string;
  password: string;
}


