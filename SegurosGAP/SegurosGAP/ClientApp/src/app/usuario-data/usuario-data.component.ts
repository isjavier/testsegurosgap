import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-usuario-data',
  templateUrl: './usuario-data.component.html',
  styles: []
})
export class UsuarioDataComponent {
  public formData: Usuario[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Usuario[]>(baseUrl + 'api/Usuario').subscribe(result => {
      this.formData = result;
    }, error => console.error(error));
  }
}

interface Usuario {
  usuarioId: number;
  email: string;
  fechaCreacion: Date;
  fechaModificacion: Date;
}


