USE SegurosGAPDB
GO

IF EXISTS(SELECT 1 FROM SYS.procedures WHERE NAME = 'SP_RemoverPolizaCoberturas')
BEGIN
	DROP PROCEDURE SP_RemoverPolizaCoberturas
END
GO
CREATE PROCEDURE SP_RemoverPolizaCoberturas
--Este SP borra todas las coberturas de una p�liza por Id.
	@polizaId INTEGER
AS
BEGIN
BEGIN TRAN
		DELETE FROM Poliza_Coberturas WHERE polizaId = @polizaId
COMMIT TRAN
END
GO
IF EXISTS(SELECT 1 FROM SYS.procedures WHERE NAME = 'SP_RemoverPoliza')
BEGIN
	DROP PROCEDURE SP_RemoverPoliza
END
GO
CREATE PROCEDURE SP_RemoverPoliza
--Este SP borra una poliza y sus dependencias.
	@polizaId INTEGER
AS
BEGIN
	BEGIN TRAN
	--Este SP ya esta creado
		EXEC SP_RemoverPolizaCoberturas @polizaId
	--Delete normales
		DELETE FROM Cliente_Polizas WHERE polizaId = @polizaId
		DELETE FROM Polizas  WHERE polizaId = @polizaId
	COMMIT TRAN
END
GO
IF EXISTS(SELECT 1 FROM SYS.procedures WHERE NAME = 'SP_RemoverCliente_Poliza')
BEGIN
	DROP PROCEDURE SP_RemoverCliente_Poliza
END
GO
CREATE PROCEDURE SP_RemoverCliente_Poliza
--Este SP borra un cliente-Poliza
	@clientePolizaId INTEGER
AS
BEGIN
	BEGIN TRAN
		DELETE FROM Cliente_Polizas  WHERE clientePolizaId = @clientePolizaId
	COMMIT TRAN
END