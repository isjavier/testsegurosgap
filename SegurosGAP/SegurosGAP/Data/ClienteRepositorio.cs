﻿using SegurosGAP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Data
{
    public class ClienteRepositorio<T>: Repositorio<DetalleClientePoliza>, IClienteRepositorio<T>
    {

        public ClienteRepositorio(SegurosGAPContext context)
        {
            _context = context;
        }

        public List<DetalleClientePoliza> GetDetalleClientePoliza() {
            List<DetalleClientePoliza> detalleClientePolizas = new List<DetalleClientePoliza>();
            DetalleClientePoliza detalleClientePoliza = new DetalleClientePoliza();
            List<Cliente_Poliza> cliente_Polizas = new List<Cliente_Poliza>();
            Cliente cliente = new Cliente();
            Poliza poliza = new Poliza();

            try
            {
                cliente_Polizas = GetClientePolizas();
                if (cliente_Polizas.Count > 0)
                {
                    foreach (Cliente_Poliza item in cliente_Polizas)
                    {
                        detalleClientePoliza = new DetalleClientePoliza();

                        detalleClientePoliza.clientePolizaId = item.clientePolizaId;
                        detalleClientePoliza.inicioVigencia = item.inicioVigencia;
                        detalleClientePoliza.periodoCobertura = item.periodoCobertura;
                        detalleClientePoliza.precioTotalDolares = item.precioTotalDolares;
                        detalleClientePoliza.estadoPoliza = item.estadoPoliza;

                        cliente = GetClienteConPoliza(item.clienteId);
                        if (cliente != null) {
                            detalleClientePoliza.clienteId = cliente.clienteId;
                            detalleClientePoliza.nombreCliente = cliente.nombreCliente;
                            detalleClientePoliza.identificacion = cliente.identificacion;
                        }

                        poliza = GetPoliza(item.polizaId);
                        if (poliza != null) {
                            detalleClientePoliza.polizaId = poliza.polizaId;
                            detalleClientePoliza.nombrePoliza = poliza.nombrePoliza;
                            detalleClientePoliza.porcentajeCobertura = poliza.porcentajeCobertura;
                            detalleClientePoliza.precioMensualDolares = poliza.precioMensualDolares;
                        }
                        detalleClientePolizas.Add(detalleClientePoliza);
    }
                    }
               return detalleClientePolizas;
            }
            catch (Exception ex) {
                throw ex;
            }   
        }

        //Obtener polizas cliente
        public List<Cliente_Poliza> GetClientePolizas()
        {
            Repositorio<Cliente_Poliza> _repositorio;
            try
            {
                _repositorio = new Repositorio<Cliente_Poliza>(_context);
                return _context.Cliente_Polizas.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Obtener cliente por Id
        public Cliente GetClienteConPoliza(int idCliente)
        {
            Repositorio<Cliente> _repositorio;
            try
            {
                _repositorio = new Repositorio<Cliente>(_context);
                return _context.Clientes.Where(x=>x.clienteId == idCliente).Single();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Obtener póliza por ID
        public Poliza GetPoliza(int idPoliza)
        {
            Repositorio<Poliza> _repositorio;
            try
            {
                _repositorio = new Repositorio<Poliza>(_context);
                return _context.Polizas.Where(x => x.polizaId == idPoliza).Single();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AgregarClientePoliza(DetalleClientePoliza detalleClientePoliza) {

            Cliente_Poliza cliente_Poliza = new Cliente_Poliza();
            Repositorio<Cliente_Poliza> _repositorio;
            try
            {

                //Cargar datos en mi nueva cliente-poliza
            cliente_Poliza.clienteId = detalleClientePoliza.clienteId;
            cliente_Poliza.polizaId = detalleClientePoliza.polizaId;
            cliente_Poliza.inicioVigencia = detalleClientePoliza.inicioVigencia;
            cliente_Poliza.periodoCobertura = detalleClientePoliza.periodoCobertura;
            cliente_Poliza.precioTotalDolares = (detalleClientePoliza.precioMensualDolares * detalleClientePoliza.porcentajeCobertura);
            cliente_Poliza.estadoPoliza = true;
            cliente_Poliza.fechaCreacion = DateTime.Now;
            cliente_Poliza.fechaModificacion = DateTime.Now;

                _repositorio = new Repositorio<Cliente_Poliza>(_context);
                _repositorio.Agregar(cliente_Poliza);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
