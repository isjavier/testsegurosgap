﻿using Microsoft.EntityFrameworkCore;
using SegurosGAP.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Data
{
    public class Repositorio<T>: IRepositorio<T> where T: new()
    {
        internal SegurosGAPContext _context;
        public Repositorio(SegurosGAPContext context)
        {
        _context = context;
        }

        public Repositorio()
        {
                
        }
        
        public void Actualizar(T entidad)
        {
            try
            {
                using (var db = _context)
                {
                    db.Entry(entidad).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public void Agregar(T entidad)
        {
            try
            {
               
                using (var db = _context)
                {
                   
                    db.Entry(entidad).State = EntityState.Added;
                    db.SaveChanges();
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public void Eliminar(int id)
        {
            using (var db = _context)
            {
                
                db.SaveChanges();
            }
        }

        //public int ValidarUsuarioLogin(LoginUsuario loginUsuario)
        //{
        //    int usuarioId = 1;
         
      
        //    try
        //    {
        //        return usuarioId;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public List<Poliza_Cobertura> ObtenerCoberturasDePoliza(int id)
        {
            Repositorio<Poliza_Cobertura> _repositorio;

            try
            {
                _repositorio = new Repositorio<Poliza_Cobertura>(_context);
                return _repositorio._context.Set<Poliza_Cobertura>().Where(x => x.polizaId == id).ToList();            
            }
            catch (Exception ex) {
                throw ex;
            }
        }

       
        public void ActualizarCoberturasPoliza(int polizaId, List<Poliza_Cobertura> newListCoberturas)
        {
            Repositorio<Poliza_Cobertura> _repositorio;
            List<Poliza_Cobertura> oldListCoberturas = new List<Poliza_Cobertura>();
            
            try
            {
                _repositorio = new Repositorio<Poliza_Cobertura>(_context);
                using (var db = _repositorio._context)
                {
                    oldListCoberturas = ObtenerCoberturasDePoliza(polizaId);
                    if (oldListCoberturas.Count > 0)
                    {
                        db.Database.ExecuteSqlCommand("SP_RemoverPolizaCoberturas @p0", polizaId);
                     
                    }

                    if (newListCoberturas.Count > 0) {
                        foreach (Poliza_Cobertura item in newListCoberturas)
                        {
                            db.Set<Poliza_Cobertura>().Add(item);
                            db.SaveChanges();
                        }
                    }

                    }
                _repositorio._context.Dispose();

            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public int ValidarUsuario(LoginUsuario loginUsuario)
        {

            try
            {
                return _context.Set<Usuario>().Where(x => x.email == loginUsuario.email && x.password == loginUsuario.password).Count();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoverGenericoConStoredProcedure(string SP_Nombre,int id)
        {
            try
            {
                using (var db = _context)
                {

                    {
                        db.Database.ExecuteSqlCommand(SP_Nombre, id);
                        
                    }
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }
      
    }
}
