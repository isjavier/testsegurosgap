﻿using SegurosGAP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Data
{
 public interface IClienteRepositorio<T>
    {
        List<DetalleClientePoliza> GetDetalleClientePoliza();
        List<Cliente_Poliza> GetClientePolizas();
        Cliente GetClienteConPoliza(int idCliente);
        Poliza GetPoliza(int idPoliza);

    }
}
