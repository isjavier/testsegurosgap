﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SegurosGAP.Data;
using SegurosGAP.Models;

namespace SegurosGAP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PolizaController : ControllerBase
    {
        private readonly Repositorio<Poliza> _repositorio;

        public PolizaController(SegurosGAPContext context)
        {
            //_context = context;
            _repositorio = new Repositorio<Poliza>(context);
        }


        // GET: api/Poliza
        [HttpGet]
        public IEnumerable<Poliza> GetPolizas()
        {
            return _repositorio._context.Polizas;
        }

        // POST: api/Poliza
        [HttpPost]
        public async Task<IActionResult> PostAgregarPoliza([FromBody] Poliza poliza)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            
            //Validar reglas de negocio
            Poliza miPoliza = new Poliza();
            if (miPoliza.ValidarRiesgoPorcentage(poliza))
            {                
                poliza.fechaCreacion = DateTime.Now;
                poliza.fechaModificacion = DateTime.Now;
                poliza.porcentajeCobertura = miPoliza.TransformarPorcentaje(poliza.porcentajeCobertura);
                //Agregar póliza
                _repositorio.Agregar(poliza);
            }

            return CreatedAtAction("GetPoliza", new { id = poliza.polizaId }, poliza);
        }

        // GET: api/Poliza/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPoliza([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var poliza = await _repositorio._context.Polizas.FindAsync(id);

            if (poliza == null)
            {
                return NotFound();
            }

            return Ok(poliza);
        }

        // PUT: api/Poliza/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPoliza([FromRoute] int id, [FromBody] Poliza poliza)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != poliza.polizaId)
            {
                return BadRequest();
            }
            //Actualizar póliza
            _repositorio.Actualizar(poliza);
           
            return NoContent();
        }

        // DELETE: api/Poliza/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePoliza([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var poliza = await _repositorio._context.Polizas.FindAsync(id);
            if (poliza == null)
            {
                return NotFound();
            }

            _repositorio.RemoverGenericoConStoredProcedure("SP_RemoverPoliza @p0", id);

            return Ok(poliza);
        }

    }
}