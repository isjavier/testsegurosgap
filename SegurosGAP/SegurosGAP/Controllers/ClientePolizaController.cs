﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SegurosGAP.Data;
using SegurosGAP.Models;

namespace SegurosGAP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientePolizaController : ControllerBase
    {

        private readonly ClienteRepositorio<DetalleClientePoliza> _repositorio;
        public ClientePolizaController(SegurosGAPContext context)
        {
            _repositorio = new ClienteRepositorio<DetalleClientePoliza>(context);
        }

        //GET: api/ClientePoliza
        [HttpGet]
        public List<DetalleClientePoliza> GetClientePolizas()
        {
            return _repositorio.GetDetalleClientePoliza();
        }


        // POST: api/ClientePoliza
        [HttpPost]
        public async Task<IActionResult> PostAgregarClientePoliza([FromBody] DetalleClientePoliza detalleCliente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

              _repositorio.AgregarClientePoliza(detalleCliente);
        
            return CreatedAtAction("GetClientePoliza", new {}, detalleCliente);
        }

        // DELETE: api/ClientePoliza/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClientePoliza([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cliente_Poliza = await _repositorio._context.Cliente_Polizas.FindAsync(id);
            if (cliente_Poliza == null)
            {
                return NotFound();
            }

            _repositorio.RemoverGenericoConStoredProcedure("SP_RemoverCliente_Poliza @p0", id);

            return Ok(cliente_Poliza);
        }

    }
}