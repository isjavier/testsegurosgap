﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SegurosGAP.Data;
using SegurosGAP.Models;

namespace SegurosGAP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly Repositorio<Cliente> _repositorio;

        public ClienteController(SegurosGAPContext context)
        {
            //_context = context;
            _repositorio = new Repositorio<Cliente>(context);
        }

        // GET: api/Clientes
        [HttpGet]
        public IEnumerable<Cliente> GetClientes()
        {
            return _repositorio._context.Clientes;
        }

    }
}