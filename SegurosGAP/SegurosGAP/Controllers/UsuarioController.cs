﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SegurosGAP.Data;
using SegurosGAP.Models;

namespace SegurosGAP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        
        private readonly Repositorio<Usuario> _repositorio;
        public UsuarioController(SegurosGAPContext context)
        {
            _repositorio = new Repositorio<Usuario>(context);
        }

        //GET: api/Usuario
        [HttpGet]
        public IEnumerable<Usuario> GetUsuarios()
        {
            return _repositorio._context.Usuarios;
        }

        // POST: api//Usuario
        [HttpPost]
        public async Task<IActionResult> PostValidarUsuario([FromBody] LoginUsuario loginUsuario)
        {
            bool usuarioValido = false;
           
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (_repositorio.ValidarUsuario(loginUsuario) <= 0)
            {
                _repositorio._context.Dispose();
            }
            else {
                usuarioValido = true;
            }

            return CreatedAtAction("GetUsuario", new {}, usuarioValido);
        }

        // GET: api/Usuario/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUsuario([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var usuario = await _repositorio._context.Usuarios.FindAsync(id);

            if (usuario == null)
            {
                return NotFound();
            }

            return Ok(usuario);
        }

        //ToDo mantenimiento de usuario.

    } 
}