﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SegurosGAP.Data;
using SegurosGAP.Models;

namespace SegurosGAP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

   
    public class CoberturaController : ControllerBase
    {
        private readonly Repositorio<Cobertura> _repositorio;

        public CoberturaController(SegurosGAPContext context)
        {
            _repositorio = new Repositorio<Cobertura>(context);
        }

        //GET: api/Cobertura
        [HttpGet]
        public IEnumerable<Cobertura> GetCoberturas()
        {
            return _repositorio._context.Coberturas;
        }

        //ToDo mantenimiento de usuario.
    }
}