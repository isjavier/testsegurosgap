﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SegurosGAP.Data;
using SegurosGAP.Models;


namespace SegurosGAP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PolizaCoberturaController : ControllerBase
    {
        private readonly Repositorio<Poliza_Cobertura> _repositorio;

        public PolizaCoberturaController(SegurosGAPContext context)
        {
            _repositorio = new Repositorio<Poliza_Cobertura>(context);
        }

        //GET: api/PolizaCobertura
        [HttpGet]
        public IEnumerable<Poliza_Cobertura> GetCoberturas()
        {
            return _repositorio._context.Poliza_Coberturas;
        }

        // GET: api/PolizaCobertura/5
        [HttpGet("{id}")]
        public IEnumerable<Poliza_Cobertura> GetPolizaCobertura([FromRoute] int id)
        {
            if (id > 0)
            {
                return _repositorio.ObtenerCoberturasDePoliza(id);
            }
            else {
                return null;
            }
        }

        // POST: api/PolizaCobertura
        [HttpPost]
        public async void PostAgregarPolizaCobertura([FromBody] List<Poliza_Cobertura> poliza_Coberturas)
        {
            int polizaId = 0;

            if (poliza_Coberturas != null && poliza_Coberturas.Count > 0) {
                polizaId = poliza_Coberturas.First().polizaId;
   
            if (ModelState.IsValid)
                {
                 _repositorio.ActualizarCoberturasPoliza(polizaId, poliza_Coberturas);
                
                }
            }
        }

    }
}