﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SegurosGAP.Migrations
{
    public partial class CreacionDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cliente_Polizas",
                columns: table => new
                {
                    clientePolizaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    clienteId = table.Column<int>(type: "integer", nullable: false),
                    polizaId = table.Column<int>(type: "integer", nullable: false),
                    inicioVigencia = table.Column<DateTime>(type: "date", nullable: false),
                    periodoCobertura = table.Column<int>(type: "integer", nullable: false),
                    precioTotalDolares = table.Column<decimal>(type: "decimal(10, 2)", nullable: false),
                    estadoPoliza = table.Column<bool>(type: "bit", nullable: false),
                    fechaCreacion = table.Column<DateTime>(type: "date", nullable: true),
                    fechaModificacion = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cliente_Polizas", x => x.clientePolizaId);
                });

            migrationBuilder.CreateTable(
                name: "Clientes",
                columns: table => new
                {
                    clienteId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    nombreCliente = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    identificacion = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    email = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    telefono = table.Column<string>(type: "varchar(25)", nullable: false),
                    direccion = table.Column<string>(type: "nvarchar(200)", nullable: false),
                    fechaCreacion = table.Column<DateTime>(type: "date", nullable: true),
                    fechaModificacion = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clientes", x => x.clienteId);
                });

            migrationBuilder.CreateTable(
                name: "Coberturas",
                columns: table => new
                {
                    coberturaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    cobertura = table.Column<string>(type: "nvarchar(150)", nullable: false),
                    fechaCreacion = table.Column<DateTime>(type: "date", nullable: true),
                    fechaModificacion = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coberturas", x => x.coberturaId);
                });

            migrationBuilder.CreateTable(
                name: "loginUsuarios",
                columns: table => new
                {
                    usuarioId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    email = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    password = table.Column<string>(type: "varchar(20)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_loginUsuarios", x => x.usuarioId);
                });

            migrationBuilder.CreateTable(
                name: "Poliza_Coberturas",
                columns: table => new
                {
                    polizaCoberturaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    polizaId = table.Column<int>(type: "integer", nullable: false),
                    coberturaId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Poliza_Coberturas", x => x.polizaCoberturaId);
                });

            migrationBuilder.CreateTable(
                name: "Polizas",
                columns: table => new
                {
                    polizaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    nombrePoliza = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    descripcion = table.Column<string>(type: "nvarchar(150)", nullable: false),
                    porcentajeCobertura = table.Column<decimal>(type: "decimal(10, 2)", nullable: false),
                    precioMensualDolares = table.Column<decimal>(type: "decimal(10, 2)", nullable: false),
                    tipoRiesgoId = table.Column<int>(type: "integer", nullable: false),
                    fechaCreacion = table.Column<DateTime>(type: "date", nullable: true),
                    fechaModificacion = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Polizas", x => x.polizaId);
                });

            migrationBuilder.CreateTable(
                name: "Tipo_Riesgos",
                columns: table => new
                {
                    tipoRiesgoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    tipoRiesgo = table.Column<string>(type: "varchar(25)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tipo_Riesgos", x => x.tipoRiesgoId);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    usuarioId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    email = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    password = table.Column<string>(type: "varchar(20)", nullable: false),
                    fechaCreacion = table.Column<DateTime>(type: "date", nullable: true),
                    fechaModificacion = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.usuarioId);
                });

            migrationBuilder.InsertData(
                table: "Cliente_Polizas",
                columns: new[] { "clientePolizaId", "clienteId", "estadoPoliza", "fechaCreacion", "fechaModificacion", "inicioVigencia", "periodoCobertura", "polizaId", "precioTotalDolares" },
                values: new object[,]
                {
                    { 1, 1, true, new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), 8, 1, 444m },
                    { 2, 2, true, new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), 24, 2, 1687.2m }
                });

            migrationBuilder.InsertData(
                table: "Clientes",
                columns: new[] { "clienteId", "direccion", "email", "fechaCreacion", "fechaModificacion", "identificacion", "nombreCliente", "telefono" },
                values: new object[,]
                {
                    { 1, "Ciudad 1, Avenida 1, Calle 1, Casa 1.", "Ana@email.com", new DateTime(2019, 8, 27, 4, 24, 27, 534, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 534, DateTimeKind.Local), "1111-1111", "Ana Segura Montes", "(506)9999-9999" },
                    { 2, "Ciudad 2, Avenida 2, Calle 2, Casa 2.", "Juan@email.com", new DateTime(2019, 8, 27, 4, 24, 27, 534, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 534, DateTimeKind.Local), "2222-2222", "Juan Solís Herrera", "(506)8888-8888" }
                });

            migrationBuilder.InsertData(
                table: "Coberturas",
                columns: new[] { "coberturaId", "cobertura", "fechaCreacion", "fechaModificacion" },
                values: new object[,]
                {
                    { 1, "Terremoto", new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local) },
                    { 2, "Incendio", new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local) },
                    { 3, "Robo", new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local) },
                    { 4, "Pérdida", new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local) },
                    { 5, "Inundación", new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local) }
                });

            migrationBuilder.InsertData(
                table: "Poliza_Coberturas",
                columns: new[] { "polizaCoberturaId", "coberturaId", "polizaId" },
                values: new object[,]
                {
                    { 4, 4, 2 },
                    { 3, 3, 2 },
                    { 1, 1, 1 },
                    { 2, 2, 1 }
                });

            migrationBuilder.InsertData(
                table: "Polizas",
                columns: new[] { "polizaId", "descripcion", "fechaCreacion", "fechaModificacion", "nombrePoliza", "porcentajeCobertura", "precioMensualDolares", "tipoRiesgoId" },
                values: new object[,]
                {
                    { 2, "Esta póliza protege contra robo y pérdida.", new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), "Póliza contra robo y pérdida.", 60m, 70.3m, 3 },
                    { 1, "Esta póliza protege contra terremoto e incendio.", new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 535, DateTimeKind.Local), "Póliza contra terremoto e incendio.", 80.5m, 55.5m, 2 }
                });

            migrationBuilder.InsertData(
                table: "Tipo_Riesgos",
                columns: new[] { "tipoRiesgoId", "tipoRiesgo" },
                values: new object[,]
                {
                    { 1, "Bajo" },
                    { 2, "Medio" },
                    { 3, "Medio-alto" },
                    { 4, "Alto" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "usuarioId", "email", "fechaCreacion", "fechaModificacion", "password" },
                values: new object[,]
                {
                    { 1, "Usuario1@email.com", new DateTime(2019, 8, 27, 4, 24, 27, 532, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 534, DateTimeKind.Local), "123456" },
                    { 2, "Usuario2@email.com", new DateTime(2019, 8, 27, 4, 24, 27, 534, DateTimeKind.Local), new DateTime(2019, 8, 27, 4, 24, 27, 534, DateTimeKind.Local), "123456" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cliente_Polizas");

            migrationBuilder.DropTable(
                name: "Clientes");

            migrationBuilder.DropTable(
                name: "Coberturas");

            migrationBuilder.DropTable(
                name: "loginUsuarios");

            migrationBuilder.DropTable(
                name: "Poliza_Coberturas");

            migrationBuilder.DropTable(
                name: "Polizas");

            migrationBuilder.DropTable(
                name: "Tipo_Riesgos");

            migrationBuilder.DropTable(
                name: "Usuarios");
        }
    }
}
