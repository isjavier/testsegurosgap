﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Models
{
    public class Cobertura
    {
        [Key]
        public int coberturaId { get; set; }

        [Required]
        [Column(TypeName ="nvarchar(150)")]
        public string cobertura { get; set; }

        [Column(TypeName = "date")]
        public System.Nullable<DateTime> fechaCreacion { get; set; }

        [Column(TypeName = "date")]
        public System.Nullable<DateTime> fechaModificacion { get; set; }
    }
}
