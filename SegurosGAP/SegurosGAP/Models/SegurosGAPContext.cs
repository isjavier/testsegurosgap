﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Models
{
       /*  
          * Agregar la referencia de Microsoft entity framework core via NutGet Packages,
          * verificar la versión del proyecto deben ser las mismas (2.1.1).
       */
    public class SegurosGAPContext:DbContext
    {

        public SegurosGAPContext(DbContextOptions<SegurosGAPContext> options) : base(options)
        {

        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Poliza> Polizas { get; set; }
        public DbSet<Cobertura> Coberturas { get; set; }
        public DbSet<Tipo_Riesgo> Tipo_Riesgos { get; set; }
        public DbSet<Cliente_Poliza> Cliente_Polizas { get; set; }
        public DbSet<Poliza_Cobertura> Poliza_Coberturas { get; set; }
        public DbSet<LoginUsuario> loginUsuarios  { get; set; }

        /*
         * Seeding de datos. Datos por defecto insertados en la base de datos
         * Comando para migration: Add-Migration "CreacionDB"
         * Comando para undo migration: Remove-Migration
         * Comando para update DB: Update-Database
         */
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Usuario
            modelBuilder.Entity<Usuario>().HasData(
                new Usuario
                {
                    usuarioId =1,                
                    email = "Usuario1@email.com",
                    password = "123456",
                    fechaCreacion = DateTime.Now,
                    fechaModificacion = DateTime.Now
                },
                new Usuario
                {
                    usuarioId = 2,
                    email = "Usuario2@email.com",
                    password = "123456",
                    fechaCreacion = DateTime.Now,
                    fechaModificacion = DateTime.Now
                });
            //Cliente
            modelBuilder.Entity<Cliente>().HasData(
                new Cliente
                {
                    clienteId = 1,
                    nombreCliente = "Ana Segura Montes",
                    identificacion = "1111-1111",
                    email = "Ana@email.com",
                    telefono = "(506)9999-9999",
                    direccion = "Ciudad 1, Avenida 1, Calle 1, Casa 1.",
                    fechaCreacion = DateTime.Now,
                    fechaModificacion = DateTime.Now
                },
                new Cliente
                {
                    clienteId = 2,
                    nombreCliente = "Juan Solís Herrera",
                    identificacion = "2222-2222",
                    email = "Juan@email.com",
                    telefono = "(506)8888-8888",
                    direccion = "Ciudad 2, Avenida 2, Calle 2, Casa 2.",
                    fechaCreacion = DateTime.Now,
                    fechaModificacion = DateTime.Now
                });
            //Tipo_Riesgo
            modelBuilder.Entity<Tipo_Riesgo>().HasData(
                new Tipo_Riesgo
                {
                    tipoRiesgoId = 1,
                    tipoRiesgo = "Bajo"
                },
                new Tipo_Riesgo
                {
                    tipoRiesgoId = 2,
                    tipoRiesgo = "Medio"
                },
                new Tipo_Riesgo
                {
                    tipoRiesgoId = 3,
                    tipoRiesgo = "Medio-alto"
                },
                new Tipo_Riesgo
                {
                    tipoRiesgoId = 4,
                    tipoRiesgo = "Alto"
                });
            //Poliza
            modelBuilder.Entity<Poliza>().HasData(
               new Poliza
               {
                   polizaId = 1,
                   nombrePoliza = "Póliza contra terremoto e incendio.",
                   descripcion = "Esta póliza protege contra terremoto e incendio.",
                   porcentajeCobertura = 80.5,
                   precioMensualDolares = 55.50,
                   tipoRiesgoId = 2,
                   fechaCreacion = DateTime.Now,
                   fechaModificacion = DateTime.Now
               },
               new Poliza
               {
                   polizaId = 2,
                   nombrePoliza = "Póliza contra robo y pérdida.",
                   descripcion = "Esta póliza protege contra robo y pérdida.",
                   porcentajeCobertura = 60,
                   precioMensualDolares = 70.30,
                   tipoRiesgoId = 3,
                   fechaCreacion = DateTime.Now,
                   fechaModificacion = DateTime.Now
               });
            //Cobertura
            modelBuilder.Entity<Cobertura>().HasData(
              new Cobertura
              {
                  coberturaId = 1,
                  cobertura = "Terremoto",
                  fechaCreacion = DateTime.Now,
                  fechaModificacion = DateTime.Now
              },
              new Cobertura
              {
                  coberturaId = 2,
                  cobertura = "Incendio",
                  fechaCreacion = DateTime.Now,
                  fechaModificacion = DateTime.Now
              },
              new Cobertura
              {
                  coberturaId = 3,
                  cobertura = "Robo",
                  fechaCreacion = DateTime.Now,
                  fechaModificacion = DateTime.Now
              },
              new Cobertura
              {
                  coberturaId = 4,
                  cobertura = "Pérdida",
                  fechaCreacion = DateTime.Now,
                  fechaModificacion = DateTime.Now
              },
              new Cobertura
              {
                  coberturaId = 5,
                  cobertura = "Inundación",
                  fechaCreacion = DateTime.Now,
                  fechaModificacion = DateTime.Now
              });
            //Poliza_Cobertura
            modelBuilder.Entity<Poliza_Cobertura>().HasData(
              new Poliza_Cobertura
              {
                  polizaCoberturaId = 1,
                  polizaId = 1,
                  coberturaId = 1
              },
              new Poliza_Cobertura
              {
                  polizaCoberturaId = 2,
                  polizaId = 1,
                  coberturaId = 2
              },
              new Poliza_Cobertura
              {
                  polizaCoberturaId = 3,
                  polizaId = 2,
                  coberturaId = 3
              },
              new Poliza_Cobertura
              {
                  polizaCoberturaId = 4,
                  polizaId = 2,
                  coberturaId = 4
              });
            //Cliente_Poliza
            modelBuilder.Entity<Cliente_Poliza>().HasData(
              new Cliente_Poliza
              {
                  clientePolizaId = 1,
                  clienteId = 1,
                  polizaId = 1,
                  inicioVigencia = DateTime.Now,
                  periodoCobertura = 8,
                  precioTotalDolares = (8 * 55.50), //meses * precio póliza mensual
                  estadoPoliza = true,
                  fechaCreacion = DateTime.Now,
                  fechaModificacion = DateTime.Now
              },
              new Cliente_Poliza
              {
                  clientePolizaId = 2,
                  clienteId = 2,
                  polizaId = 2,
                  inicioVigencia = DateTime.Now,
                  periodoCobertura = 24,
                  precioTotalDolares = (24 * 70.30), //meses * precio póliza mensual
                  estadoPoliza = true,
                  fechaCreacion = DateTime.Now,
                  fechaModificacion = DateTime.Now
              });
        }
    }
}
