﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Models
{
    public class Poliza
    {
        [Key]
        public int polizaId { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public String nombrePoliza { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(150)")]
        public string descripcion { get; set; }

        [Required]
        [Column(TypeName = "decimal(10, 2)")]
        public double porcentajeCobertura { get; set; }

        [Required]
        [Column(TypeName = "decimal(10, 2)")]
        public double precioMensualDolares { get; set; }

        [ForeignKey("Tipo_Riesgo")]
        [Required]
        [Column(TypeName = "integer")]
        public int tipoRiesgoId { get; set; }

        [Column(TypeName = "date")]
        public System.Nullable<DateTime> fechaCreacion { get; set; }

        [Column(TypeName = "date")]
        public System.Nullable<DateTime> fechaModificacion { get; set; }


        //Reglas de negocio para pólizas
        public bool ValidarRiesgoPorcentage(Poliza poliza) {
            double porcentajeMensual = TransformarPorcentaje(poliza.porcentajeCobertura);
            if (poliza.tipoRiesgoId == Convert.ToInt32(ETipoRiesgo.alto) && porcentajeMensual > 0.5)
            {
                return false;
            }
            return true;
        }

        public double TransformarPorcentaje(double porcentaje)
        {
            return (porcentaje / 100);
        }

        enum ETipoRiesgo { bajo = 1, medio = 2, medioAlto = 3, alto = 4 };
    }
}
