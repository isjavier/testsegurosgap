﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Models
{
    [NotMapped]
    public class DetalleClientePoliza
    {
        public int clientePolizaId { get; set; }
        public DateTime inicioVigencia { get; set; }
        public int periodoCobertura { get; set; }
        public double precioTotalDolares { get; set; }
        public bool estadoPoliza { get; set; }

        public int clienteId { get; set; }
        public String nombreCliente { get; set; }
        public string identificacion { get; set; }

        public int polizaId { get; set; }
        public String nombrePoliza { get; set; }
        public double porcentajeCobertura { get; set; }
        public double precioMensualDolares { get; set; }
    }
}
