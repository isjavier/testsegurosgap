﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Models
{
    public class Cliente
    {
        [Key]
       // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int clienteId { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public String nombreCliente { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(30)")]
        public string identificacion { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string email { get; set; }

        [Required]
        [Column(TypeName = "varchar(25)")]
        public string telefono { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(200)")]
        public string direccion { get; set; }

        [Column(TypeName = "date")]
        public System.Nullable<DateTime> fechaCreacion { get; set; }

        [Column(TypeName = "date")]
        public System.Nullable<DateTime> fechaModificacion { get; set; }
    }
}
