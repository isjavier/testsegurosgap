﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Models
{
    public class Usuario
    {
        [Key]
        public int usuarioId { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string email { get; set; }

        [Required]
        [Column(TypeName = "varchar(20)")]
        public string password { get; set; }

        [Column(TypeName = "date")]
        public System.Nullable<DateTime> fechaCreacion { get; set; }

        [Column(TypeName = "date")]
        public System.Nullable<DateTime> fechaModificacion { get; set; }
    }
}
