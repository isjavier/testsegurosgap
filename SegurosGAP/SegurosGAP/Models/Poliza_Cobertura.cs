﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Models
{
    public class Poliza_Cobertura
    {
        [Key]
        public int polizaCoberturaId { get; set; }

        [ForeignKey("Poliza")]
        [Required]
        [Column(TypeName = "integer")]
        public int polizaId { get; set; }

        [ForeignKey("Cobertura")]
        [Required]
        [Column(TypeName = "integer")]
        public int coberturaId { get; set; }
    }
}
