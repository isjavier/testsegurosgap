﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Models
{
    public class Tipo_Riesgo
    {
        [Key]
        public int tipoRiesgoId { get; set; }

        [Required]
        [Column(TypeName = "varchar(25)")]
        public String tipoRiesgo { get; set; }
   
    }
}
