﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SegurosGAP.Models
{
    public class Cliente_Poliza
    {
        [Key]
        public int clientePolizaId { get; set; }

        [ForeignKey("Cliente")]
        [Required]
        [Column(TypeName = "integer")]
        public int clienteId { get; set; }

        [ForeignKey("Poliza")]
        [Required]
        [Column(TypeName = "integer")]
        public int polizaId { get; set; }

        [Required]
        [Column(TypeName = "date")]
        public DateTime inicioVigencia { get; set; }

        [Required]
        [Column(TypeName = "integer")]
        public int periodoCobertura { get; set; }

        [Required]
        [Column(TypeName = "decimal(10, 2)")]
        public double precioTotalDolares { get; set; }

        [Required]
        [Column(TypeName = "bit")]
        public bool estadoPoliza { get; set; }

        [Column(TypeName = "date")]
        public System.Nullable<DateTime> fechaCreacion { get; set; }

        [Column(TypeName = "date")]
        public System.Nullable<DateTime> fechaModificacion { get; set; }

    }
}
